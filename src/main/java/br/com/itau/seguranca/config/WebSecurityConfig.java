package br.com.itau.seguranca.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.itau.seguranca.security.JwtFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and().authorizeRequests()
          .antMatchers(HttpMethod.GET, "/livre").permitAll()
          .antMatchers(HttpMethod.POST, "/login").permitAll()
          .anyRequest().authenticated()
        .and().addFilterBefore(new JwtFilter(), UsernamePasswordAuthenticationFilter.class);
//        .and().httpBasic();
  }
  
//  @Override
//  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//    auth.inMemoryAuthentication()
//      .withUser("admin")
//      .password(bcryptEncoder().encode("admin123"))
//      .roles("ADMIN");
//  }
//  
//  @Bean
//  protected BCryptPasswordEncoder bcryptEncoder() {
//    return new BCryptPasswordEncoder();
//  }
}

package br.com.itau.seguranca.controllers;

import java.security.Principal;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.seguranca.security.JwtTokenProvider;

@RestController
public class ExemploController {
  @GetMapping("/livre")
  public String livre() {
    return "endpoint sem autenticação";
  }
  
  @GetMapping("/admin")
  public String admin(Principal principal) {
    return "endpoint autenticado" + principal.getName();
  }
  
  @PostMapping("/login")
  public String login(@RequestBody Map<String, String> login) {
    if(login.get("usuario").equals("admin") && login.get("senha").equals("senha123")) {
      JwtTokenProvider tokenProvider = new JwtTokenProvider();
      
      return tokenProvider.criarToken("admin");
    }
    
    throw new ResponseStatusException(HttpStatus.FORBIDDEN);
  }
}
